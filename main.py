import pygame
import random
# initation of Pygame ;D
pygame.init()

screen = pygame.display.set_mode((400, 800))
clock = pygame.time.Clock()
sky_surface = pygame.image.load("sprites/sky.jpeg").convert()
ground = pygame.image.load("sprites/ground.png").convert_alpha()
retry_img = pygame.image.load("sprites/retry.png")
retry_rect = retry_img.get_rect(topleft = (133, 550))

exit_img = pygame.image.load("sprites/exit.png")
exit_rect = retry_img.get_rect(topleft = (250, 250))

font = pygame.font.Font(None, 50)
text_end = font.render("Game Over\n Try again ?", None, "Black")

score = font.render("Score :", False, "Black")
score_rect = score.get_rect(center = (200, 400)) 

mob = pygame.image.load("sprites/mobv2.png").convert_alpha()
mob_rect = mob.get_rect(topleft = (200, 0))

player = pygame.image.load("sprites/player.png").convert_alpha()
player_rect = player.get_rect(topleft = (200, 650))

attack = pygame.image.load("sprites/attack.png")
pygame.display.set_caption("Dollop")


def gameOver():
    mouse_pos = pygame.mouse.get_pos()
    if mob_rect.colliderect(player_rect):
        pygame.event.clear()
        screen.blit(text_end, (200, 400))
        screen.blit(exit_img, exit_rect)
        pygame.event.wait(6000)
        if exit_rect.collidepoint(mouse_pos):
            if pygame.mouse.get_pressed()[0] == True:
                exit()

def playerMouvement():
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player_rect.centerx -= 5
    if keys[pygame.K_RIGHT]:
        player_rect.centerx += 5
    if keys[pygame.K_SPACE]:
        screen.blit(attack, attack_rect)
        if attack_rect.colliderect(mob_rect):
            mob_rect.x = random.randint(0, 350)
            attack_rect.y = player_rect.y
        attack_rect.x -= 1

    
# Game Loop
while True: 
    attack_rect = attack.get_rect(topleft = (player_rect.centerx, player_rect.top))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
    # Showing
    screen.blit(sky_surface, (0, 0))
    screen.blit(ground, (0, 544))
    screen.blit(mob, mob_rect)
    screen.blit(score, score_rect)
    screen.blit(player, player_rect)
    
    # Mouvement
    mob_rect.y += random.randint(1, 5) 
    if mob_rect.y > 850:
        mob_rect.y = 0
        mob_rect.x = random.randint(0, 350)
    
    if mob_rect.colliderect(attack_rect):
        mob_rect.centery = 0 

    # collision
    
    playerMouvement()
    gameOver()
    pygame.display.update()
    clock.tick(60)
